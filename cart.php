<?php 
include('header.php');
if(isset($_SESSION['loggedin_user']))
{
?>
<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<script src="assets/js/bootstrap.min.js"></script>
<div id="wrapper" class="container">
	<section class="header_text sub">
		<img class="pageBanner" style="width: 1170px;height: 183px">
		<h4 style="padding-top: 15px;"><span>Shopping Cart</span></h4>
	</section>
	<section class="main-content">				
		<div class="">
			<form method="post" action="checkout.php">
				<div class="span12">					
					<h4 class="title"><span class="text"><strong>Your</strong> Cart</span></h4>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Remove</th>
								<th>Image</th>
								<th>Product Name</th>
								<th>Quantity</th>
								<th>Unit Price</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody id="cart_items">
							
						</tbody>
					</table>
					<hr>
					<p class="cart-total right" id="total_prices">
					</p>
					<hr>
						<center><button class="btn btn-inverse" type="submit" id="checkout">Checkout</button></center>
				</div>
			</form>
		</div>
	</section>
</div>
<script type="text/javascript">
var cart = [];
var token = '';
var cart_contents = [];
var total = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
	{
	?>
	token = "<?=$_SESSION['loggedin_user']['token'];?>";
	$.ajax({
			url:'<?=$url;?>loadCart',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				transaction_type:'delivery'
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					total = result.details.cart_details.cart_subtotal;
					var data = result.details.data.item;
					var array = [];
					var array = Object.values(data);
					if(array.length > 0)
						{
						for (var i = 0;i < array.length; i++)
							{
							cart_contents.push(array[i]);
							cart.push(array[i].item_id);
							}
						}
					}
				}
		});
	<?php
	}
?>
$(document).ready(function() 
{ 
$.ajax({
        url:'<?=$url;?>getMerchantInfo',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1'
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            $('.pageBanner').attr('alt',result.details.data.merchant_name);
            $('.pageBanner').attr('src',result.details.data.logo);
            }
          else
            location.reload();
          }
      });
var html = '';
$.ajax({
		url:'<?=$url;?>loadCategory',
		async: false,
		data:
			{
			merchant_keys:'7034288755',
			device_id:'XXX_1234567890_1230187',
			device_platform:'android',
			device_uiid:'uid_123',
			code_version:'1.2',
			lang:'en',
			search_mode:'address',
			location_mode:'1',
			},
		dataType:'json',
		success:function(result)
			{
			if(result.code == 1)
				{
				var cat = result.details.data;
				var html1_product = '';
				var row = 1;
				for (var i = 0; i < cat.length; i++) 
					{
					$.ajax({
							url:'<?=$url;?>loadItemByCategory',
							async: false,
							data:
								{
								cat_id:cat[i].cat_id,
								merchant_keys:'7034288755',
						          device_id:'XXX_1234567890_1230187',
						          device_platform:'android',
						          device_uiid:'uid_123',
						          code_version:'1.2',
						          lang:'en',
						          search_mode:'address',
						          location_mode:'1'
								},
							dataType:'json',
							success:function(result1)
								{
								if(result1.code == 1)
									{
									var items = result1.details.data;
									for (var j = 0; j < items.length; j++) 
										{
										if(cart.indexOf(items[j].item_id) != -1)
											{
											var qty = get_qty(items[j].item_id);
											var amount = get_amount(items[j].item_id,qty);
											html = html+`
	<tr>
		<td>
			<input type="checkbox" onclick="remove_cart(`+items[j].item_id+`)">
		</td>
		<td>
			<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
				<img src="`+items[j].photo_url+`" alt="`+items[j].item_name+`">
			</a>
		</td>
		<td>
			`+items[j].item_name+`
		</td>
		<td>
			<input type="hidden" name="item[]" value="`+items[j].item_id+`">
			<input type="number" id="qty_`+items[j].item_id+`" min="1" class="input-mini" value="`+qty+`" onchange="change_qty(`+items[j].item_id+`,`+items[j].prices[0].price+`,this.value)">
		</td>
		<td>
			`+items[j].prices[0].formatted_price+`
		</td>
		<td>
			$ `+parseFloat(amount)+`
		</td>
	</tr>`;
											row++;
											}
										}
									}
								}
							});
					}
				html = html+`<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><strong>$`+parseFloat(total).toFixed(2)+`</strong></td></tr>`;
				$('#cart_items').html('');
				$('#cart_items').html(html);
				var html1 = `<strong>Sub-Total</strong>:$ `+parseFloat(total).toFixed(2)+`<br><strong>Total</strong>: $ `+parseFloat(total).toFixed(2)+`<br>`;
				$('#total_prices').html();
				$('#total_prices').html(html1);
				}
			}
		});
});
function get_qty(item_id)
	{
	for (var i = 0; i < cart_contents.length; i++)
		{
		if(cart_contents[i].item_id == item_id)
			{
			return cart_contents[i].qty;
			}
		}
	}
function get_amount(item_id,qty)
	{
	for (var i = 0; i < cart_contents.length; i++)
		{
		if(cart_contents[i].item_id == item_id)
			{
			var amount = parseFloat(cart_contents[i].discounted_price) * parseFloat(qty);
			return parseFloat(amount);
			}
		}
	}
function change_qty(id,price,qty)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>addToCart?&merchant_keys=7034288755&device_id=XXX_1234567890_1230187&device_platform=android&device_uiid=uid_123&code_version=1.2&lang=en&search_mode=address&location_mode=1',
			method:'post',
			data:
				{
				item_id:parseInt(id),
				token:token,
				qty:qty,
				price:parseFloat(price),
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#cart_items').load('#cart_items');
					$('#total_prices').load('#total_prices');
					}
				}
			});
	}
function remove_cart(row)
	{
	$.ajax({
			url:'<?=$url;?>removeCartitem',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				row:row,
				},
			dataType:'json',
			success:function(result)
				{
				$('#cart_items').load('#cart_items');
				$('#total_prices').load('#total_prices');
				}
		});
	}
</script>
<?php 
include('footer.php');
}
else
{
echo("<script>location.href = 'index.php';</script>");
}