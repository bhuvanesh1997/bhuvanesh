<?php 
if(isset($_GET['cat']))
{
include('header.php');
?>
<style type="text/css">
	.product_img
		{
		height: 194px;
    	width: 266px;
		}
</style>
<div class="uk-section uk-dark uk-background-cover" style="background-image: url('assets/images/breadcrumbs.jpg')">
  	<div class="uk-container uk-container-xlarge">
    	<h2 class="uk-heading-small" id="cat_name"></h2> 
    	<ul class="uk-breadcrumb">
      		<li class="uk-button uk-button-text"><a href="index.php">Home</a></li>
      		<li><span id="cat_name1"></span></li>
    	</ul>          
  	</div>
</div>
<div class="uk-section uk-section-muted">
	<div class="uk-container uk-container-xlarge">
		<div class="uk-child-width-1-4@m uk-grid" uk-grid="" id="products">
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function()
{
$.ajax({
		url:'<?=$url;?>loadCategory',
		async: false,
		data:
			{
			merchant_keys:'7034288755',
			device_id:'XXX_1234567890_1230187',
			device_platform:'android',
			device_uiid:'uid_123',
			code_version:'1.2',
			lang:'en',
			search_mode:'address',
			location_mode:'1'
			},
		dataType:'json',
		success:function(result)
			{
			if(result.code == 1)
				{
				$('#cat_name').html('');
				$('#cat_name1').html('');
				var cat = result.details.data;
				for (var i = 0; i < cat.length; i++) 
					{
					if(cat[i].cat_id == '<?=$_GET['cat'];?>')
						{
						$('#cat_name').html(cat[i].category_name);
						$('#cat_name1').html(cat[i].category_name);
						var html_product = '';
						$.ajax({
								url:'<?=$url;?>loadItemByCategory',
								async: false,
								data:
									{
									cat_id:cat[i].cat_id,
									merchant_keys:'7034288755',
							        device_id:'XXX_1234567890_1230187',
							        device_platform:'android',
							        device_uiid:'uid_123',
							        code_version:'1.2',
							        lang:'en',
							        search_mode:'address',
							        location_mode:'1'
									},
								dataType:'json',
								success:function(result1)
									{
									if(result1.code == 1)
										{
										var items = result1.details.data;
										for (var j = 0; j < items.length; j++) 
											{
											html_product = html_product+`<div class="uk-first-column"><div class="uk-card uk-border-10 toggle uk-animation-toggle" tabindex="0"><div class="uk-card-media-top uk-list-three"><a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`"><img src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border"></a></div><div class="uk-list-two"><h3 class="uk-card-title"><a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`" class="uk-link-heading">`+items[j].item_name+`</a><form method="post" action="" class="uk-display-flex"><input type="number" name="Post_id" hidden="" value="`+items[j].item_id+`"><button class="uk-float-right uk-color-heart uk-icon" type="submit" uk-icon="icon: heart"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="heart"><path fill="none" stroke="#000" stroke-width="1.03" d="M10,4 C10,4 8.1,2 5.74,2 C3.38,2 1,3.55 1,6.73 C1,8.84 2.67,10.44 2.67,10.44 L10,18 L17.33,10.44 C17.33,10.44 19,8.84 19,6.73 C19,3.55 16.62,2 14.26,2 C11.9,2 10,4 10,4 L10,4 Z"></path></svg></button></form></h3><ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top"><li><a href="" class="uk-color-2"><span uk-icon="icon: clock; ratio: 0.7" class="uk-icon"><svg width="14" height="14" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="clock"><circle fill="none" stroke="#000" stroke-width="1.1" cx="10" cy="10" r="9"></circle><rect x="9" y="4" width="1" height="7"></rect><path fill="none" stroke="#000" stroke-width="1.1" d="M13.018,14.197 L9.445,10.625"></path></svg></span> `+items[j].prices[0].formatted_price+`</a></li><li class="uk-margin-top"><a class="uk-cat" href="category.php?cat=`+cat[i].cat_id+`>`+cat[i].category_name+`</a></li><li class="uk-margin-top"><a href="" class="uk-color-2 uk-animation-slide-left-small uk-text-15"><img src="https://www.freepik.com/free-icon/add-shopping-cart-e-commerce-button_782753.htm">Add</a></li></ul></div></div></div>`;
											}
										$('#products').html(html_product);
										}
									else
										{
										$('#products').html('No Products Found');	
										}
									}
							});
						break;
						}
					}
				}
			else
				{
				alert("asdas");
				$('#cat_name').html('');
				$('#cat_name1').html('');
				$('#products').html('');	
				$('#products').html('No Products Found');	
				}
			}
	});
})
</script>
<?php 
include('footer.php');
}
else
{
header('Location: index.php');
}
?>