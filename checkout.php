<?php 
include('header.php');
if(isset($_POST['item']))
	{
	if(isset($_SESSION['loggedin_user']))
		{
		?>
<link href="assets/css/boostrap1.min.css" rel="stylesheet">      
<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">		
<link href="assets/css/bootstrappage.css" rel="stylesheet"/>
<link href="assets/css/main.css" rel="stylesheet"/>
<script src="assets/js/bootstrap.min.js"></script>	
<style>
.myDiv 
	{
	border: 5px;
	text-align: center;
	}
</style>
<div id="wrapper" class="container">
	<section class="header_text sub">
		<img class="pageBanner" style="width: 1170px;height: 183px">
		<h4><span>Check Out</span></h4>
	</section>	
	<section class="main-content">
		<div class="uk-alert-success" uk-alert id="msg" style="display: none;">
			<a class="uk-alert-close" uk-close></a>
			<p id="message"></p>
		</div>
		<form id="checkout" method="get">
			<div class="row">
				<div class="span12">
					<div class="accordion" id="accordion1">
						<div class="accordion-group">
							<div class="accordion-heading">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne">Account &amp; Billing Details</a>
							</div>
							<div id="collapseOne" class="accordion-body collapse in">
								<div class="accordion-inner">
									<div class="myDiv">
										<h4>Choose a delivery address</h4>
										<span id="delivery_address"></span>
									</div>
									<div class="row-fluid">
										<div class="span12">
											<h4>Address</h4>
											<div class="uk-alert-success" uk-alert id="msg" style="display: none;">
								  				<a class="uk-alert-close" uk-close></a>
								  				<p id="message"></p>
											</div>
											<?php 
											for ($i=0; $i < count($_POST['item']); $i++) 
												{
												$item = $_POST['item'][$i];
												echo "<input type='hidden' name='item[]' value=".$item.">";
												}
											?>
											<div class="control-group span6">
												<label class="control-label">First Name</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="first_name" id="first_name">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Last Name</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="last_name" id="last_name">
												</div>
											</div>					  
											<div class="control-group span6">
												<label class="control-label">House/Office/Building Name</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="location_name" id="location_name">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Landmark/Town</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="street" id="street">
												</div>
											</div>
											<div class="control-group span6">
												<label class="control-label">Contact Number</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="contact_phone" id="contact_phone">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Password</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="password" id="password">
												</div>
											</div>
											<div class="control-group span6">
												<label class="control-label">Confirm Password</label>
												<div class="controls">
													<input type="text" placeholder="" class="input-xlarge" name="cpassword" id="cpassword">
												</div>
											</div>
											<div class="control-group">
												<label class="control-label">Save Address</label>
												<div class="controls">
													<input type="checkbox" placeholder="" class="input-xlarge" name="save_address" id="save_address" value="1">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<center><button class="btn btn-inverse" id="confirm_order">Confirm order</button></center>
					</div>
				</div>	
			</div>
		</form>
	</section>
</div>
<script type="text/javascript">
var token = "<?=$_SESSION['loggedin_user']['token'];?>";
$(document).ready(function() 
{
$.ajax({
        url:'<?=$url;?>getMerchantInfo',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1'
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            $('.pageBanner').attr('alt',result.details.data.merchant_name);
            $('.pageBanner').attr('src',result.details.data.logo);
            }
          else
            location.reload();
          }
      });
$.ajax({
        url:'<?=$url;?>getAddressBookDropDown',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1',
          token:token,
          transaction_type:'delivery',
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 6)
          	{
          	$('#delivery_address').html(result.details.title);
          	}
          }
      });
});
$('#confirm_order').on('click',function()
	{
	var fname = $('#first_name').val();
	if(fname == '')
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Enter the first name.');
		return false;
		}
	var location_name = $('#location_name').val();
	if(location_name == '')
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Enter the location.');
		return false;
		}
	var street = $('#street').val();
	if(street == '')
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Enter the street.');
		return false;
		}
	var contact_phone = $('#contact_phone').val();
	if(contact_phone == '')
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Enter the contact phone.');
		return false;
		}
	var password = $('#password').val();
	if(password == '')
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Enter the password.');
		return false;
		}
	var cpassword = $('#cpassword').val();
	if(cpassword == '')
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Enter the Confirm password.');
		return false;
		}
	if(cpassword != password)
		{
		$('#msg').show();
		$('#message').html('');
		$('#message').html('Password Missmatch.');
		return false;
		}
	var lname = $('#last_name').val();
	var save_address = $('#save_address').val();
	$.ajax({
		    url:'<?=$url;?>setDeliveryAddress',
			async: false,
		    data:
		      {
		      merchant_keys:'7034288755',
		      device_id:'XXX_1234567890_1230187',
		      device_platform:'android',
		      device_uiid:'uid_123',
		      code_version:'1.2',
		      lang:'en',
		      search_mode:'address',
		      location_mode:'1',
		      token:token,
		      lat:'12345',
		      lng:'12345',
		      search_address2:'',
		      mapbox_drag_map:true,
		      mapbox_drag_end:true,
		      last_name:lname,
		      cpassword:cpassword,
		      first_name:fname,
		      location_name:location_name,
		      street:street,
		      contact_phone:contact_phone,
		      password:password,
		      delivery_instruction:'',
		      save_address:save_address,
		      transaction_type:'delivery',
		      },
		    dataType:'json',
		    success:function(result)
		      	{
		      	if(result.code != 1)
		      		{
		      		$('#msg').show();
		      		$('#message').html('');
		      		$('#message').html(result.msg);
		      		}
		      	else if(result.code == 1)
		      		{
		      		$.ajax({
						    url:'<?=$url;?>payNow',
							async: false,
						    data:
						      {
						      merchant_keys:'7034288755',
						      device_id:'XXX_1234567890_1230187',
						      device_platform:'android',
						      device_uiid:'uid_123',
						      code_version:'1.2',
						      lang:'en',
						      search_mode:'address',
						      location_mode:'1',
						      token:token,
						      transaction_type:'delivery',
						      payment_provider:'cod',
						      delivery_date:"<?=date('Y-m-d');?>",
						      order_change:"undefined",
						      delivery_asap:true
						      },
						    dataType:'json',
						    success:function(result)
						      	{
						      	if(result.code == 1)
						      		{
						      		Swal.fire('Success!','Your Order Has Placed Successfully!','success')
						      		setTimeout(function()
						      			{
   										window.location="index.php";
										}, 5000);
						      		}

						      	}
						  });
		      		}
		      	}
		});
	});
 $("form").submit(function(e){
        e.preventDefault();
    });
</script>
	<?php 
		}
	else
		{
		echo("<script>location.href = 'index.php';</script>");
		}
	}
else
	{
	echo("<script>location.href = 'index.php';</script>");
	}
include('footer.php');
?>