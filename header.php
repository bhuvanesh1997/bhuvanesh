<!DOCTYPE html>
<html lang="en-gb" dir="ltr">
<head>
<?php 
session_start();
$url = "http://localhost/bhuvanesh/calicut/sevendelivery/singlemerchant/api/";
?>
 <!-- ==================== Meta site =================== -->
 <meta charset="utf-8">
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
 <!-- ====== Laravel description site edit delete from admin panel ================== -->
 <meta name="description" content="Avocada - Food Delivery Restaurant Directory and Online Ordering script Template">
 <!-- ====== Laravel author site edit delete from admin panel ====================== -->
 <meta name="author" content="author">
 <!-- ====== Laravel keywords site edit delete from admin panel ================== -->
 <meta name="keywords" content="Avocada - Food Delivery Restaurant Directory and Online Ordering script Template">  
 <!-- ====== Laravel robots site edit delete from admin panel ================== -->
 <meta name="robots" content="Metarobots">
 <meta name="msapplication-TileColor" content="#ffffff">
 <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
 <meta name="theme-color" content="#ffffff">
 <!-- TWITTER META -->
 <meta name="twitter:card" content="summary">
 <meta name="twitter:site" content="@Avocada - Food Delivery">
 <meta name="twitter:creator" content="@Avocada - Food Delivery">
 <meta name="twitter:title" content="Avocada - Food Delivery">
 <meta name="twitter:description" content="Avocada - Food Delivery">
 <!-- ====== Laravel favicon icon ================== -->
 <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-icon-57x57.png">
 <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon/apple-icon-60x60.png">
 <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-icon-72x72.png">
 <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-icon-76x76.png">
 <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-icon-114x114.png">
 <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-icon-120x120.png">
 <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-icon-144x144.png">
 <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-icon-152x152.png">
 <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-icon-180x180.png">
 <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon/android-icon-192x192.png">
 <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-32x32.png">
 <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-32x32.png">
 <link rel="manifest" href="assets/images/favicon/manifest.json">
 <!-- ====== SiteTitle ======= -->
 <title>Food Delivery</title>
 <!-- =====================================  googleapis =================================== -->
 <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600&display=swap" rel="stylesheet"> 
 <!-- =====================================  googleapis =================================== -->
 <link href="https://fonts.googleapis.com/css?family=Righteous&display=swap" rel="stylesheet">
 <!-- =====================================  googleapis =================================== -->
 <link href="https://fonts.googleapis.com/css2?family=Lalezar&display=swap" rel="stylesheet">
 <!-- =====================================  main =================================== -->
 <link rel="stylesheet" type="text/css" href="assets/asset/css/main.css" />
 <!-- =========================================  uikit ============================== -->
 <script src="assets/asset/js/uikit.js"></script>
 <!-- ===================================  uikit icons ============================== -->
 <script src="assets/asset/js/uikit-icons.js"></script> 
 <!-- ===========================================  head =================================== -->
 <span class="display-zero"><!-- {{ $Locale = LaravelLocalization::getCurrentLocale() }} --></span>
 <link rel="stylesheet" href="assets/asset/css/rtl.css" />
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
 <style type="text/css">
   .modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
 </style>
</head>
<body>
<div class="modal"><!-- Place at bottom of page --></div>
<!-- =====================================  content =================================== -->
<nav class="uk-navbar-container uk-letter-spacing-small">
  <div class="uk-container uk-container-xlarge">
  	<div class="uk-position-z-index" data-uk-navbar>
      <div class="uk-navbar-left">
        <a class="uk-navbar-item uk-logo" href="index.php" id="image"></a>
       	<ul class="uk-navbar-nav uk-visible@m uk-margin-large-left">
        	<li>
           	<a href="index.php">Home</a>
    			</li>
        	<li>
           	<a href="contact.php">Contact</a>
          </li>
    			<li aria-expanded="false" class="">
      			<a type="button"> Category<span class="uk-label uk-label-success uk-label-menu">Shop</span></a>
        	</li>
        	<div uk-dropdown="" class="uk-dropdown uk-dropdown-bottom-left" style="left: 560.469px; top: 126px;">
            <ul class="uk-nav uk-dropdown-nav" id="order_now_menu"></ul>
        	</div>
        </ul>
    	</div>
    	<div class="uk-navbar-right uk-margin-small-right">
        <?php 
        if(!isset($_SESSION['loggedin_user']))
          {
          ?>
      		<ul class="uk-thumbnav">
        	  <li>
              <a type="button">
            	 <img class="uk-comment-avatar uk-border-rounded" src="assets/images/avatar.png" alt="avatar" width="40" height="40">
          		</a>
        		</li>
      		</ul>
      		<div uk-dropdown="mode: click ;pos: bottom-right">
        	  <ul class="uk-nav uk-dropdown-nav">
              <li>
          	    <a href="login.php"><span uk-icon="sign-in"></span>Sign In</a>
          		</li>
          		<li>
          		  <a href="register.php"><span uk-icon="user"></span>Sign Up</a>
          		</li>
        		</ul>
      		</div>
          <?php 
          }
        else
          {
          ?> 
    		  <ul class="uk-thumbnav">
      			<li>
        			<a type="button">
          			<img class="uk-comment-avatar uk-border-rounded" src="<?=$_SESSION['loggedin_user']['avatar'];?>" alt="avatar" width="40" height="40">
        			</a>
      			</li>
    		  </ul>
      		<div uk-dropdown="mode: click ;pos: bottom-right">
        		<ul class="uk-nav uk-dropdown-nav">
              <li>
                <a href="user.php?user=<?=$_SESSION['loggedin_user']['email_address'];?>"><span uk-icon="user"></span> <?=$_SESSION['loggedin_user']['first_name']." ".$_SESSION['loggedin_user']['last_name'];?></a>
              </li>
              <li class="uk-nav-divider"></li>
              <li>
                <a href="cart.php" id="cart_count_menu"><span uk-icon="cart"></span> My Cart</a>
              </li>
              <li class="uk-nav-divider"></li>
              <li>
                <a href="products.php?i=4"><span uk-icon="heart"></span> Favorites</a>
              </li>
              <li class="uk-nav-divider"></li>
              <li>
                <a href="order.php"><span uk-icon="happy"></span> Orders</a>
              </li>
              <li class="uk-nav-divider"></li>
              <li>
                <a href="logout.php" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><span uk-icon="sign-out"></span> Sign Out</a>
                <form id="logout-form" action="logout.php" method="POST" class="display-zero"></form>
              </li> 
      		  </ul>
    		  </div>
          <?php 
          }
          ?>
        <a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas" data-uk-toggle><span data-uk-navbar-toggle-icon></span></a>
      </div>
    </div>
  </div>
</nav>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
  <?php
  if(isset($_SESSION['loggedin_user']))
    {
    ?>
    $.ajax({
        url:'<?=$url;?>getCartCount',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1',
          token:'<?=$_SESSION["loggedin_user"]["token"];?>',
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            var html = '<span uk-icon="cart"></span> My Cart ('+result.details.count+')';
            $('#cart_count_menu').html(html)
            }
          }
      });

    <?php 
    }
  ?>
	$(document).ready(function() 
		{ 
		$.ajax({
				url:'<?=$url;?>loadCategory',
				data:
					{
					merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1'
					},
				dataType:'json',
				success:function(result)
					{
					if(result.code == 1)
						{
						$('#order_now_menu').html('');
						var cat = result.details.data;
						var html = ""; 
						for (var i = 0; i < cat.length; i++) 
							{
							html = html+`<li><a href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
							}
						$('#order_now_menu').html(html);
						}
					else
						location.reload();
					}
		  });
    $.ajax({
        url:'<?=$url;?>getMerchantInfo',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1'
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            $('#image').html('');
            var image = `<img style="width:205px;height:40px" alt="`+result.details.data.merchant_name+`" src="`+result.details.data.logo+`">`
            $('#image').html(image);
            }
          else
            location.reload();
          }
      });
    
		});
</script>