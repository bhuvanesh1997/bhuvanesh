<?php include('header.php');?>
<style type="text/css">
	.product_img
		{
		height: 194px;
    	width: 266px;
		}
</style>
<!--********************* SITE CONTENT *********************-->
<span class="display-zero"><!-- {{ $Locale = LaravelLocalization::getCurrentLocale() }} --></span>
<!-- =======================  content ======================= -->
<div class="uk-section uk-section-muted uk-padding-remove-top">
	<div class="uk-container uk-container-xlarge">
		<div class="uk-border-rounded uk-background-cover uk-panel" id="banner">
			<div data-uk-grid="" class="uk-grid uk-grid-stack">
				<div class="uk-width-1-1 uk-first-column">
					<div class="inner-header uk-padding-large uk-dark" style="height: 330px; padding-top: 140px; padding-left: 33px; padding-bottom: 20px">
						<h1 style="color: white" class="uk-heading-medium uk-margin-remove-top uk-animation-slide-left-small">Delisious <span class="uk-Montserrat">Food items,</span></h1>
						<h1 style="color: white" class="uk-heading-medium uk-margin-remove-top uk-animation-slide-left-small"><span class="uk-Montserrat">Delivered</span> To You. </h1>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<div class="uk-section uk-section-muted uk-padding-remove-top">
	<div class="uk-container uk-container-xlarge">
		<div class="uk-width-expand">
			<div  uk-grid>
				<div class="uk-width-1-2">
					<h1 class="uk-heading-xsmall uk-margin-remove-top uk-heading-bullet">Our Food Items</h1>
				</div>
				<div class="uk-width-1-2">
					<div class="uk-text-right uk-dark">
						<!-- ===================== Search form ============================ -->
						<form class="uk-search uk-search-default uk-search-large" method="GET" action="products.php" role="search" name="myForm" onsubmit="return validateForm()" >
							<button uk-search-icon></button>
							<input type="hidden" name="i" value="5">
							<input class="uk-search-input uk-text-emphasis uk-border-pill" type="search" placeholder="Food You Wish To Have" name="search" type="search">
						</form>
					</div> 
				</div>
			</div>
			<div class="uk-alert-success" uk-alert id="msg" style="display: none;">
  				<a class="uk-alert-close" uk-close></a>
  				<p id="message"></p>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<ul class="uk-subnav uk-subnav-pill" uk-switcher="animation: uk-animation-slide-top-medium, uk-animation-slide-right-medium" id="category">
				</ul>
				<ul class="uk-switcher" id="products" style="touch-action: pan-y pinch-zoom;">

				</ul>
				<div class="col-md-12"><center><a href="products.php?i=1" class="uk-cat">Show More</a></center></div>
			</div>
		</div>
	</div>
</div>
<div class="uk-container uk-container-xlarge">
	<div class="uk-width-expand">
		<div uk-grid="" class="uk-grid">
			<div class="uk-width-1-2 uk-first-column">
				<h1 class="uk-heading-xsmall uk-margin-remove-top uk-heading-bullet">Featured Categories</h1>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<ul class="uk-switcher" style="touch-action: pan-y pinch-zoom;">
					<li class="uk-active" style="">
						<div class="uk-child-width-1-4@m uk-grid" uk-grid="" id="featured_category">
							
						</div>
					</li>
				</ul>
				<div class="col-md-12"><center><a href="products.php?i=2" class="uk-cat">Show More</a></center></div>
			</div>
		</div>
	</div>
</div>
<div class="uk-container uk-container-xlarge">
	<div class="uk-width-expand">
		<div uk-grid="" class="uk-grid">
			<div class="uk-width-1-2 uk-first-column">
				<h1 class="uk-heading-xsmall uk-margin-remove-top uk-heading-bullet">Deals</h1>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<ul class="uk-switcher" style="touch-action: pan-y pinch-zoom;">
					<li class="uk-active" style="">
						<div class="uk-child-width-1-4@m uk-grid" uk-grid="" id="deal_product">
							
						</div>
					</li>
				</ul>
				<div class="col-md-12"><center><a href="products.php?i=3" class="uk-cat">Show More</a></center></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var favorite = [];
var cart = [];
var token = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
	{
	?>
	token = "<?=$_SESSION['loggedin_user']['token'];?>";
	// get favorite list if logged in
	$.ajax({
			url:'<?=$url;?>ItemFavoritesList',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data;
					if(data.length > 0)
						{
						for (var i = 0;i < data.length; i++)
							{
							favorite.push(data[i].item_id);
							}
						}
					}
				}
		});
	// get the items in cart if logged in
	$.ajax({
			url:'<?=$url;?>loadCart',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				transaction_type:'delivery'
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data.item;
					var array = [];
					var array = Object.values(data);
					if(array.length > 0)
						{
						for (var i = 0;i < array.length; i++)
							{
							cart.push(array[i].item_id);
							}
						}
					}
				}
		});
	<?php 
	}
?>
</script>
<script type="text/javascript">
$(document).ready(function() 
{
// load the category for listing the product
$.ajax({
	url:'<?=$url;?>loadCategory',
	async: false,
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			$('#category').html('');
			$('#featured_category').html('');
			var cat = result.details.data;
			var html_cat = ``;
			var html1_product = '';
			var featured_category = '';
			var deal_product = '';
			var count = 0;
			for (var i = 0; i < cat.length; i++) 
				{
				if(i == 0)
					html1_product = html1_product+`<li class="uk-active"><div class="uk-child-width-1-4@m uk-grid" uk-grid="">`;
				else
					html1_product = html1_product+`<li><div class="uk-child-width-1-4@m uk-grid" uk-grid="">`;
				if(i == 0)
					{
					html_cat = html_cat+`<li class="uk-active"><a class="switcher-Foods" href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
					}
				else
					{
					html_cat = html_cat+`<li class="uk-active"><a class="switcher-Foods" href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
					}
				$('#category').html(html_cat);
				// load the items for the categories
				$.ajax({
					url:'<?=$url;?>loadItemByCategory',
					async: false,
					data:
						{
						cat_id:cat[i].cat_id,
						merchant_keys:'7034288755',
				          device_id:'XXX_1234567890_1230187',
				          device_platform:'android',
				          device_uiid:'uid_123',
				          code_version:'1.2',
				          lang:'en',
				          search_mode:'address',
				          location_mode:'1'
						},
					dataType:'json',
					success:function(result1)
						{
						if(result1.code == 1)
							{
							var items = result1.details.data;
							for (var j = 0; j < items.length; j++) 
								{
								if(favorite.indexOf(items[j].item_id) == -1)
									{
    								var button = `<button type="submit" onclick="set_favorite(`+items[j].item_id+`,`+cat[i].cat_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i></button>&nbsp;`;
									} 
								else
									{
									var button = `<button type="submit" onclick="remove_favorite(`+items[j].item_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart" aria-hidden="true"></i></button>&nbsp;`;
									}
								if(cart.indexOf(items[j].item_id) == -1)
									{	
									button = button+`<button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" onclick="add_tocart(`+items[j].item_id+`,`+items[j].prices[0].price+`)">Add To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>`;
									}
								else
									{
									button = button+`<a href="cart.php" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small">Go To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>`;
									}
								if(items[j].deal_status == 1)
									{
									deal_product = deal_product+`
<div class="uk-first-column">
	<div class="uk-card uk-border-10 toggle uk-animation-toggle" tabindex="0">
		<div class="uk-card-media-top uk-list-three">
			<a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`">
				<img style="width:266px;height:275px" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border">
			</a>
		</div>
		<div class="uk-list-two">
			<h3 class="uk-card-title">
				<a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`" class="uk-link-heading">`+items[j].item_name+`</a>
			</h3>
			<ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
				<li>`+items[j].prices[0].formatted_price+`</li>
				<li class="uk-margin-top">
					<a  href="category.php?cat=`+cat[i].cat_id+`"class="uk-cat">`+cat[i].category_name+`</a>
				</li>
				<li class="uk-margin-top" id="bottons_`+items[j].item_id+`">`
					+button+
				`</li>
			</ul>
		</div>
	</div>
</div>`;
								}
							if(j<7)
								{
html1_product = html1_product+`
<div class="uk-first-column">
	<div class="uk-card uk-border-10 toggle uk-animation-toggle" tabindex="0">
		<div class="uk-card-media-top uk-list-three">
			<a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`">
				<img style="width:266px;height:275px" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border">
			</a>
		</div>
		<div class="uk-list-two">
			<h3 class="uk-card-title">
				<a href="product_view.php?product=`+items[j].item_id+`&cat=`+cat[i].cat_id+`" class="uk-link-heading">`+items[j].item_name+`</a>
			</h3>
			<ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
				<li>`+items[j].prices[0].formatted_price+`</li>
				<li class="uk-margin-top">
					<a  href="category.php?cat=`+cat[i].cat_id+`"class="uk-cat">`+cat[i].category_name+`</a>
				</li>
				<li class="uk-margin-top" id="bottons_`+items[j].item_id+`">`
					+button+
				`</li>
			</ul>
		</div>
	</div>
</div>`;
									}
								}
							}
						}
					});
				html1_product = html1_product+`</div></li>`;
				}
			$('#products').html("");
			$('#products').html(html1_product);
			$('#deal_product').html(deal_product);
			var x = 0;
			for (var i = 0; i < cat.length; i++) 
				{
				if(cat[i].featured_cat == 1)
					{
					if(x<7)
						{
						x++;
						featured_category = featured_category+`<div class="">
									<div class="uk-card uk-border-10 toggle uk-animation-toggle" tabindex="0">
										<div class="uk-card-media-top uk-list-three">
											<a href="category.php?cat=`+cat[i].cat_id+`">
												<img style="width:266px;height:275px" src="`+cat[i].photo_url+`" alt="`+cat[i].category_name+`" class="uk-card-border">
											</a>
										</div>
										<div class="uk-list-two">
											<h3 class="uk-card-title">
												<a href="category.php?cat=`+cat[i].cat_id+`" class="uk-link-heading">`+cat[i].category_name+`</a>
											</h3>
										</div>
									</div>
								</div>`;
						}
					}
				}
			$('#featured_category').html(featured_category);
			}
		}
	});
$.ajax({
	url:'<?=$url;?>getAppSettings',
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			var html = result.details.singleapp_banner[0];
			document.getElementById('banner').style.backgroundImage="url("+html+")";
			}
		}
	});

});
function set_favorite(item_id,cat_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>AddFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				category_id:cat_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#bottons_'+item_id).load('#bottons_'+item_id);
				}
			});
	}
function remove_favorite(item_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>RemoveFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html('Removed from your favorite list.');
					}
				$('#bottons_'+item_id).load('#bottons_'+item_id);
				}
			});
	}
function add_tocart(item_id,price)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			// url:'<?=$url;?>addToCart',
			// url:'cart.php',
			url:'<?=$url;?>addToCart?&merchant_keys=7034288755&device_id=XXX_1234567890_1230187&device_platform=android&device_uiid=uid_123&code_version=1.2&lang=en&search_mode=address&location_mode=1',
			method:'post',
			data:
				{
				item_id:parseInt(item_id),
				token:token,
				qty:1,
				price:parseFloat(price),
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#bottons_'+item_id).load('#bottons_'+item_id);
				}
			});
	}
</script>
<?php include('footer.php');?>