<?php 
if(isset($_GET['cat']))
  {
  if(isset($_GET['product']))
    {
    include('header.php');
    ?>
    <div class="uk-section uk-dark uk-background-cover" style="background-image: url('assets/images/breadcrumbs.jpg')">
      <div class="uk-container uk-container-xlarge">
        <h2 class="uk-heading-small" id="product_name"></h2> 
        <ul class="uk-breadcrumb">
            <li class="uk-button uk-button-text"><a href="index.php">Home</a></li>
            <li class="" id="cat_name"></li>
        </ul>          
      </div>
    </div>
    <div class="uk-section uk-section-muted">
      <div class="uk-container uk-container-xlarge">
        <div data-uk-grid="" class="uk-grid">
          <div class="uk-width-expand uk-first-column">
            <img class="uk-border-10" width="100%" uk-img="" id="product_image">
            <div class="uk-card uk-card-default uk-card-body uk-border-10 uk-margin">
              <div uk-grid="" class="uk-grid uk-grid-stack">
                <div class="uk-width-1-1 uk-first-column">
                  <article class="uk-article">
                    <ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
                      <li>
                        <a class="uk-color-2">
                          <span id="product_price"></span>
                        </a>
                      </li>
                      <li id="cat_name1"></li>
                    </ul>
                    <p id="product_detail"></p>
                  </article>
                </div>
              </div>
            </div>
          </div>
          <div class="uk-width-1-3@m">
            <div class="uk-card uk-card-default uk-card-body uk-border-rounded">
              <div uk-grid="" class="uk-grid uk-grid-stack">
                <div class="uk-width-expand uk-first-column">
                  <div class="uk-grid-small uk-child-width-auto uk-grid uk-grid-stack" uk-grid="">
                    <div class="uk-margin-small-share uk-first-column">
                      <h1 class="uk-heading-xsmall uk-margin-remove-top uk-heading-bullet uk-text-20">Share This Food :</h1>
                    </div>
                    <div class="uk-grid-margin uk-first-column">
                      <a href="http://www.facebook.com/sharer.php?u=https://localhost/bhuvanesh/calicut/awad" class="uk-icon-button uk-icon" uk-icon="facebook" target="_blank"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="facebook"><path d="M11,10h2.6l0.4-3H11V5.3c0-0.9,0.2-1.5,1.5-1.5H14V1.1c-0.3,0-1-0.1-2.1-0.1C9.6,1,8,2.4,8,5v2H5.5v3H8v8h3V10z"></path></svg></a>
                      <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=https://localhost/bhuvanesh/calicut/awad" class="uk-icon-button uk-icon" uk-icon="linkedin"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="linkedin"><path d="M5.77,17.89 L5.77,7.17 L2.21,7.17 L2.21,17.89 L5.77,17.89 L5.77,17.89 Z M3.99,5.71 C5.23,5.71 6.01,4.89 6.01,3.86 C5.99,2.8 5.24,2 4.02,2 C2.8,2 2,2.8 2,3.85 C2,4.88 2.77,5.7 3.97,5.7 L3.99,5.7 L3.99,5.71 L3.99,5.71 Z"></path><path d="M7.75,17.89 L11.31,17.89 L11.31,11.9 C11.31,11.58 11.33,11.26 11.43,11.03 C11.69,10.39 12.27,9.73 13.26,9.73 C14.55,9.73 15.06,10.71 15.06,12.15 L15.06,17.89 L18.62,17.89 L18.62,11.74 C18.62,8.45 16.86,6.92 14.52,6.92 C12.6,6.92 11.75,7.99 11.28,8.73 L11.3,8.73 L11.3,7.17 L7.75,7.17 C7.79,8.17 7.75,17.89 7.75,17.89 L7.75,17.89 L7.75,17.89 Z"></path></svg></a>
                      <a href="http://reddit.com/submit?url=https://localhost/bhuvanesh/calicut/awad&amp;title=Share" class="uk-icon-button uk-icon" uk-icon="reddit" target="_blank"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="reddit"><path d="M19 9.05a2.56 2.56 0 0 0-2.56-2.56 2.59 2.59 0 0 0-1.88.82 10.63 10.63 0 0 0-4.14-1v-.08c.58-1.62 1.58-3.89 2.7-4.1.38-.08.77.12 1.19.57a1.15 1.15 0 0 0-.06.37 1.48 1.48 0 1 0 1.51-1.45 1.43 1.43 0 0 0-.76.19A2.29 2.29 0 0 0 12.91 1c-2.11.43-3.39 4.38-3.63 5.19 0 0 0 .11-.06.11a10.65 10.65 0 0 0-3.75 1A2.56 2.56 0 0 0 1 9.05a2.42 2.42 0 0 0 .72 1.76A5.18 5.18 0 0 0 1.24 13c0 3.66 3.92 6.64 8.73 6.64s8.74-3 8.74-6.64a5.23 5.23 0 0 0-.46-2.13A2.58 2.58 0 0 0 19 9.05zm-16.88 0a1.44 1.44 0 0 1 2.27-1.19 7.68 7.68 0 0 0-2.07 1.91 1.33 1.33 0 0 1-.2-.72zM10 18.4c-4.17 0-7.55-2.4-7.55-5.4S5.83 7.53 10 7.53 17.5 10 17.5 13s-3.38 5.4-7.5 5.4zm7.69-8.61a7.62 7.62 0 0 0-2.09-1.91 1.41 1.41 0 0 1 .84-.28 1.47 1.47 0 0 1 1.44 1.45 1.34 1.34 0 0 1-.21.72z"></path><path d="M6.69 12.58a1.39 1.39 0 1 1 1.39-1.39 1.38 1.38 0 0 1-1.38 1.39z"></path><path d="M14.26 11.2a1.39 1.39 0 1 1-1.39-1.39 1.39 1.39 0 0 1 1.39 1.39z"></path><path d="M13.09 14.88a.54.54 0 0 1-.09.77 5.3 5.3 0 0 1-3.26 1.19 5.61 5.61 0 0 1-3.4-1.22.55.55 0 1 1 .73-.83 4.09 4.09 0 0 0 5.25 0 .56.56 0 0 1 .77.09z"></path></svg></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="uk-card uk-card-default uk-card-body uk-border-rounded uk-margin-top">
              <div uk-grid="" class="uk-grid uk-grid-stack">
                <div class="uk-width-1-1 uk-first-column">
                  <div class="uk-width-expand">
                    <h4 class="uk-comment-title"><a class="uk-link-reset">Add To Cart</a></h4>
                    <p class="uk-comment-meta uk-margin-remove"><a class="uk-color-list"></a></p>
                  </div> 
                </div>
                <div class="uk-width-expand uk-margin-top uk-grid-margin uk-first-column">
                  <form method="post" action="" name="Order" onsubmit="return Order()">
                    <div class="uk-inline uk-width">
                      <input class="uk-input " type="number" min="1" value="1" placeholder="Qty" name="qty">
                    </div>
                    <button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i></button>&nbsp;<button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small">Add To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function()
      {
      $.ajax({
            url:'<?=$url;?>loadCategory',
            async: false,
            data:
              {
              merchant_keys:'7034288755',
              device_id:'XXX_1234567890_1230187',
              device_platform:'android',
              device_uiid:'uid_123',
              code_version:'1.2',
              lang:'en',
              search_mode:'address',
              location_mode:'1'
              },
            dataType:'json',
            success:function(result)
              {
              if(result.code == 1)
                {
                var cat = result.details.data;
                for (var i = 0; i < cat.length; i++) 
                  {
                  if(cat[i].cat_id == '<?=$_GET['cat'];?>')
                    {
                    var cat_name = `<a href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a>`;
                    $('#cat_name').html('');
                    $('#cat_name').html(cat_name);
                    $.ajax({
                            url:'<?=$url;?>loaditemDetails',
                            async: false,
                            data:
                              {
                              merchant_keys:'7034288755',
                              device_id:'XXX_1234567890_1230187',
                              device_platform:'android',
                              device_uiid:'uid_123',
                              code_version:'1.2',
                              lang:'en',
                              search_mode:'address',
                              location_mode:'1',
                              item_id:"<?=$_GET['product'];?>",
                              cat_id:"<?=$_GET['cat'];?>"
                              },
                            dataType:'json',
                            success:function(result1)
                              {
                              if(result1.code == 1)
                                {
                                $('#product_name').html('');
                                $('#product_name').html(result1.details.data.item_name);
                                var src = result1.details.data.photo;
                                var alt = result1.details.data.item_name;
                                var data_src = result1.details.data.photo;
                                $("#product_image").attr("src", src);
                                $("#product_image").attr("alt", alt);
                                $("#product_image").attr("data-src", data_src);
                                var html = `<a class="uk-cat" href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a>`;
                                $('#cat_name1').html('');
                                $('#cat_name1').html(html);
                                $('#product_price').html('');
                                $('#product_price').html(result1.details.data.prices[0].formatted_price);
                                $('#product_detail').html('');
                                $('#product_detail').html(result1.details.data.item_description);
                                }
                              }
                          });
                    }
                  }
                }
              }
          });
      })
    </script>
    <?php 
    include('footer.php');
    }
  else
    {
    header('Location: category.php');
    }
  }
else
  {
  header('Location: index.php');
  }
?>