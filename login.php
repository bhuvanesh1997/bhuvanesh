<?php 
$url = "http://localhost/bhuvanesh/calicut/sevendelivery/singlemerchant/api/";
session_start();
if(isset($_SESSION['loggedin_user']))
  {
  header('Location: index.php');
  }
?>
<html lang="en">
<head>
<!--============================= Meta site =============================== -->
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>Food Delivery</title>
<!-- ====== Laravel favicon icon ================== -->
<link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon-32x32.png">
<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="assets/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="assets/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen">
<link href="assets/css/select2.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="assets/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen">
<link href="assets/css/icons.css" rel="stylesheet" type="text/css" class="main-stylesheet">
<link href="assets/css/style.css" rel="stylesheet" type="text/css" class="main-stylesheet">  
</head>
<body class="fixed-header">
<!-- BEGIN SIDEBPANEL-->
<span class="display-zero">en</span>
<div class="login-wrapper">
  <!-- START Login Background Pic Wrapper-->
  <div class="bg-pic">
    <!-- START Background Pic-->
    <img src="assets/images/5ed8e3f679a374.jpg" data-src="assets/images/5ed8e3f679a374.jpg" class="lazy">
    <!-- END Background Pic-->
    <!-- START Background Caption-->
    <!-- START Background Caption-->
    <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
      <h2 class="semi-bold text-white" id="merchant_name">Food Delivery</h2>
    </div>
    <!-- END Background Caption-->
  </div>
  <!-- END Login Background Pic Wrapper-->
  <!-- START Login Right Container-->
 <div class="login-container bg-white">
  <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
    <img id="registre_image" style="width: 205px;height: 40px;">
    <p class="p-t-35">Sign Up</p>
    <p class="p-t-35" id="msg" style="display: none;color: red"></p>
    <!-- START Login Form -->
    <form class="p-t-15" role="form" method="GET" id="form_login">
      <div class="form-group form-group-default">
        <label>Sign In</label>
        <div class="controls">
          <input id="email" type="email" class="form-control " name="email" value="" placeholder="E-Mail Address" required="" autocomplete="email" autofocus="">
        </div>
      </div>
      <!-- END Form Control-->
      <!-- START Form Control-->
      <div class="form-group form-group-default">
        <label>Password</label>
        <div class="controls">
          <input id="password" type="password" class="form-control " name="password" required="" autocomplete="current-password" placeholder="Password">
        </div>
      </div>
      <!-- START Form Control-->
      <div class="row">
        <div class="col-md-12 d-flex align-items-center justify-content-end">
          <a href="password_reset.php" class="text-info small">Forgot Your Password?</a>
        </div>
      </div>
      <!-- END Form Control-->
      <button class="btn btn-primary btn-cons m-t-10" type="submit" id="signin">Sign in</button>
    </form>
  </div>
</div>
<!-- END Login Right Container-->
</div>
<!-- BEGIN VENDOR JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
  {
  $.ajax({
        url:'<?=$url;?>getMerchantInfo',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1'
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            $('#registre_image').html('');
            var src = result.details.data.logo;
            var alt = result.details.data.merchant_name;
            $("#registre_image").attr("src", src);
            $("#registre_image").attr("alt", alt);
            $('#merchant_name').html('');
            $('#merchant_name').html(result.details.data.merchant_name);
            }
          else
            location.reload();
          }
      });
  });
$("form").submit(function(e)
  {
  e.preventDefault();
  });
$('#signin').click(function()
  {
  var email = document.forms["form_login"]["email"];               
  var password = document.forms["form_login"]["password"];
  var remember = document.forms["form_login"]["remember"];
  if (email.value == "")                               
    {
    $('#msg').show();
    $('#msg').html('');
    $('#msg').html("Please enter your email id.");
    return false; 
    } 
  if (password.value == "")                                   
    {
    $('#msg').show();
    $('#msg').html('');
    $('#msg').html("Please enter your password.");
    return false;  
    } 
  $.ajax({
          url:'<?=$url;?>login',
          data:
            {
            'merchant_keys':'7034288755',
            'device_id':'XXX_1234567890_1230187',
            'device_platform':'android',
            'device_uiid':'uid_123',
            'code_version':'1.2',
            'lang':'en',
            'search_mode':'address',
            'location_mode':'1',
            'username':email.value,
            'password':password.value
            },
          dataType:'json',
          success:function(result)
            {
            console.log(result);
            if(result.code == 1)
              {
              $('#msg').hide();
              $('#msg').show();
              $('#msg').html('');
              $('#msg').html(result.msg);
              document.getElementById("form_login").reset();
              $.ajax({
                      url:'<?=$url;?>getUserProfile',
                      data:
                        {
                        'merchant_keys':'7034288755',
                        'device_id':'XXX_1234567890_1230187',
                        'device_platform':'android',
                        'device_uiid':'uid_123',
                        'code_version':'1.2',
                        'lang':'en',
                        'search_mode':'address',
                        'location_mode':'1',
                        'token':result.details.token,
                        },
                      dataType:'json',
                      success:function(result1) 
                        {
                        if(result1.code == 1)
                          { 
                          $.ajax({
                                url:'set_session.php',
                                data:
                                  {
                                  'data':result1.details.data,
                                  'token':result.details.token,
                                  },
                                dataType:'json',
                                success:function(result1) 
                                  {

                                  window.location = "index.php";
                                  },
                              });
                          }
                        else
                          {
                          window.location.reload;
                          }
                        
                        }
                    });
              }
            else
              {
              $('#msg').hide();
              $('#msg').show();
              $('#msg').html('');
              $('#msg').html(result.msg);
              return false;
              }
            }
        });
  });
</script>
</body></html>