<footer class="uk-section uk-dark">
  <div class="uk-container uk-container-xlarge  uk-text-500">
    <div class="uk-child-width-1-2@s uk-child-width-1-4@m uk-grid" data-uk-grid="">
      <div class="uk-first-column">
        <a href="" class="uk-logo uk-display-block" id="footer_image">
        </a>
        <p class="uk-padding-top">
          <a href="https://www.facebook.com/" class="uk-icon-button uk-margin-small-right uk-icon" uk-icon="twitter">
            <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="twitter">
              <path d="M19,4.74 C18.339,5.029 17.626,5.229 16.881,5.32 C17.644,4.86 18.227,4.139 18.503,3.28 C17.79,3.7 17.001,4.009 16.159,4.17 C15.485,3.45 14.526,3 13.464,3 C11.423,3 9.771,4.66 9.771,6.7 C9.771,6.99 9.804,7.269 9.868,7.539 C6.795,7.38 4.076,5.919 2.254,3.679 C1.936,4.219 1.754,4.86 1.754,5.539 C1.754,6.82 2.405,7.95 3.397,8.61 C2.79,8.589 2.22,8.429 1.723,8.149 L1.723,8.189 C1.723,9.978 2.997,11.478 4.686,11.82 C4.376,11.899 4.049,11.939 3.713,11.939 C3.475,11.939 3.245,11.919 3.018,11.88 C3.49,13.349 4.852,14.419 6.469,14.449 C5.205,15.429 3.612,16.019 1.882,16.019 C1.583,16.019 1.29,16.009 1,15.969 C2.635,17.019 4.576,17.629 6.662,17.629 C13.454,17.629 17.17,12 17.17,7.129 C17.17,6.969 17.166,6.809 17.157,6.649 C17.879,6.129 18.504,5.478 19,4.74"></path>
            </svg>
          </a>
          <a href="https://www.facebook.com/" class="uk-icon-button uk-margin-small-right uk-icon" uk-icon="facebook">
            <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="facebook">
              <path d="M11,10h2.6l0.4-3H11V5.3c0-0.9,0.2-1.5,1.5-1.5H14V1.1c-0.3,0-1-0.1-2.1-0.1C9.6,1,8,2.4,8,5v2H5.5v3H8v8h3V10z"></path>
            </svg>
          </a>
          <a href="https://www.facebook.com/" class="uk-icon-button uk-margin-small-right uk-icon" uk-icon="youtube">
            <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="youtube">
              <path d="M15,4.1c1,0.1,2.3,0,3,0.8c0.8,0.8,0.9,2.1,0.9,3.1C19,9.2,19,10.9,19,12c-0.1,1.1,0,2.4-0.5,3.4c-0.5,1.1-1.4,1.5-2.5,1.6 c-1.2,0.1-8.6,0.1-11,0c-1.1-0.1-2.4-0.1-3.2-1c-0.7-0.8-0.7-2-0.8-3C1,11.8,1,10.1,1,8.9c0-1.1,0-2.4,0.5-3.4C2,4.5,3,4.3,4.1,4.2 C5.3,4.1,12.6,4,15,4.1z M8,7.5v6l5.5-3L8,7.5z"></path>
            </svg>
          </a>
          <a href="https://www.facebook.com/" class="uk-icon-button uk-margin-small-right uk-icon" uk-icon="instagram">
            <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="instagram">
              <path d="M13.55,1H6.46C3.45,1,1,3.44,1,6.44v7.12c0,3,2.45,5.44,5.46,5.44h7.08c3.02,0,5.46-2.44,5.46-5.44V6.44 C19.01,3.44,16.56,1,13.55,1z M17.5,14c0,1.93-1.57,3.5-3.5,3.5H6c-1.93,0-3.5-1.57-3.5-3.5V6c0-1.93,1.57-3.5,3.5-3.5h8 c1.93,0,3.5,1.57,3.5,3.5V14z"></path>
                <circle cx="14.87" cy="5.26" r="1.09"></circle><path d="M10.03,5.45c-2.55,0-4.63,2.06-4.63,4.6c0,2.55,2.07,4.61,4.63,4.61c2.56,0,4.63-2.061,4.63-4.61 C14.65,7.51,12.58,5.45,10.03,5.45L10.03,5.45L10.03,5.45z M10.08,13c-1.66,0-3-1.34-3-2.99c0-1.65,1.34-2.99,3-2.99s3,1.34,3,2.99 C13.08,11.66,11.74,13,10.08,13L10.08,13L10.08,13z"></path>
            </svg>
          </a>
        </p>
      </div>
      <div>
        <ul class="uk-list">
          <h3 class="uk-heading">Help</h3>
          <li><a class="uk-button uk-button-text" href="contact.php">Contact Us</a></li>
          <li><a class="uk-button uk-button-text" href="login.php">Sign In</a></li>
          <li><a class="uk-button uk-button-text" href="register.php">Sign Up</a></li>
        </ul>
      </div>
      <div>
        <ul class="uk-list">
          <h3 class="uk-heading">Categories</h3>
          <li id="footer_cat"></li>
        </ul>
      </div>
      <div>
        <ul class="uk-list">
          <h3 class="uk-heading">Our Contact</h3>
          <a class="uk-button uk-link-muted uk-padding-remove-left"><span uk-icon="mail" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="mail"><polyline fill="none" stroke="#000" points="1.4,6.5 10,11 18.6,6.5"></polyline><path d="M 1,4 1,16 19,16 19,4 1,4 Z M 18,15 2,15 2,5 18,5 18,15 Z"></path></svg></span> info@theflowershop.ae</a>
          <a class="uk-button uk-link-muted uk-padding-remove-left uk-bl"><span uk-icon="location" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="location"><path fill="none" stroke="#000" stroke-width="1.01" d="M10,0.5 C6.41,0.5 3.5,3.39 3.5,6.98 C3.5,11.83 10,19 10,19 C10,19 16.5,11.83 16.5,6.98 C16.5,3.39 13.59,0.5 10,0.5 L10,0.5 Z"></path><circle fill="none" stroke="#000" cx="10" cy="6.8" r="2.3"></circle></svg></span>  Sheikh Zayed St Sharjah UAE </a>
          <a class="uk-button uk-link-muted uk-padding-remove-left"><span uk-icon="phone" class="uk-icon"><svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" data-svg="phone"><path fill="none" stroke="#000" d="M15.5,17 C15.5,17.8 14.8,18.5 14,18.5 L7,18.5 C6.2,18.5 5.5,17.8 5.5,17 L5.5,3 C5.5,2.2 6.2,1.5 7,1.5 L14,1.5 C14.8,1.5 15.5,2.2 15.5,3 L15.5,17 L15.5,17 L15.5,17 Z"></path><circle cx="10.5" cy="16.5" r=".8"></circle></svg></span> 06 5671699</a>   
        </ul>
      </div>
    </div>
      <!-- ===========================================  content =================================== -->
    <div class="uk-text-small uk-text-muted uk-margin-medium-top">        
      <div>
        All rights reserved 2020  
        <a class="uk-link-muted" rel="alternate" hreflang="en" href="http://localhost/bhuvanesh/calicut/awad/en">
          English
        </a>
        .
        <a href="#" uk-totop="" uk-scroll="" class="uk-icon uk-totop"><svg width="18" height="10" viewBox="0 0 18 10" xmlns="http://www.w3.org/2000/svg" data-svg="totop"><polyline fill="none" stroke="#000" stroke-width="1.2" points="1 9 9 1 17 9 "></polyline></svg></a>
      </div>
    </div>
    <!-- ===========================================  content =================================== -->
  </div>
</footer>
<script type="text/javascript">
$body = $("body");
$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }    
});
$(document).ready(function() 
  { 
  $.ajax({
      url:'<?=$url;?>loadCategory',
      data:
        {
        merchant_keys:'7034288755',
        device_id:'XXX_1234567890_1230187',
        device_platform:'android',
        device_uiid:'uid_123',
        code_version:'1.2',
        lang:'en',
        search_mode:'address',
        location_mode:'1'
        },
      dataType:'json',
      success:function(result)
        {
        if(result.code == 1)
          {
          var cat = result.details.data;
          var html = ""; 
          for (var i = 0; i < cat.length; i++) 
            {
            html = html+`<li><a class="uk-button uk-button-text" href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li><li>`;
            }
          $('#footer_cat').html('');
          $('#footer_cat').html(html);
          }
        else
          location.reload();
        }
    });
  $.ajax({
        url:'<?=$url;?>getMerchantInfo',
        data:
          {
          merchant_keys:'7034288755',
          device_id:'XXX_1234567890_1230187',
          device_platform:'android',
          device_uiid:'uid_123',
          code_version:'1.2',
          lang:'en',
          search_mode:'address',
          location_mode:'1'
          },
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            $('#image').html('');
            var image = `<img style="width:205px;height:40px" alt="`+result.details.data.merchant_name+`" src="`+result.details.data.logo+`">`
            $('#image').html(image);
            $('#footer_image').html('');
            var footer_image = `<img style="width:205px;height:40px" alt="`+result.details.data.merchant_name+`" src="`+result.details.data.logo+`">`
            $('#footer_image').html(footer_image);
            
            }
          else
            location.reload();
          }
      });
  });
</script>
</body>
</html>