<?php 
include('header.php');
if(isset($_GET['i']))
	{
	if($_GET['i'])
		$i = $_GET['i'];
	else
		$i = 1;
	}
else
	{	
	$i = 1;
	}
?>
<link href="assets/asset/css/bootstrap.min.css" rel="stylesheet">      
<link href="assets/asset/css/bootstrap-responsive.min.css" rel="stylesheet">		
<link href="assets/themes/main.css" rel="stylesheet"/>
<div id="wrapper" class="container">
	<section class="header_text sub">
		<img class="pageBanner" style="width: 1170px;height: 183px">
		<h4 style="padding-top: 15px;">
			<span>
				<?php 
				if($i == 1 || $i == '')
					echo "Products";
				elseif($i == 2)
					echo "Categories";
				elseif($i == 3)
					echo "Deals";
				elseif($i == 4)
					echo "Favorites";
				elseif($i == 5)
					echo "Searched Products";
				?>
			</span>
		</h4>
	</section>
	<div class="uk-alert-success" uk-alert id="msg" style="display: none;">
		<a class="uk-alert-close" uk-close></a>
		<p id="message"></p>
	</div>		
<?php
if($i == 1 || $i == '')
{
?>
<section class="main-content">
	<div class="row">						
		<div class="span9">								
			<ul class="thumbnails listing-products" id="products">
				
			</ul>
		</div>
		<div class="span3 col">
			<div class="block">	
				<ul class="nav nav-list" id="category">
					
				</ul>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
var favorite = [];
var cart = [];
var token = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
	{
	?>
	token = "<?=$_SESSION['loggedin_user']['token'];?>";
	// get favorite list if logged in
	$.ajax({
			url:'<?=$url;?>ItemFavoritesList',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data;
					if(data.length > 0)
						{
						for (var i = 0;i < data.length; i++)
							{
							favorite.push(data[i].item_id);
							}
						}
					}
				}
		});
	// get the items in cart if logged in
	$.ajax({
			url:'<?=$url;?>loadCart',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				transaction_type:'delivery'
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data.item;
					var array = [];
					var array = Object.values(data);
					if(array.length > 0)
						{
						for (var i = 0;i < array.length; i++)
							{
							cart.push(array[i].item_id);
							}
						}
					}
				}
		});
	<?php 
	}
?>
$(document).ready(function() 
{
// load the category for listing the product
$.ajax({
	url:'<?=$url;?>loadCategory',
	async: false,
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			$('#category').html('');
			$('#products').html('');
			var cat = result.details.data;
			var html_cat = `<li class="nav-header">SUB CATEGORIES</li>`;
			var html_product = ``;
			for (var i = 0; i < cat.length; i++) 
				{
				html_cat = html_cat+`<li><a href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
				$.ajax({
						url:'<?=$url;?>loadItemByCategory',
						async: false,
						data:
							{
							cat_id:cat[i].cat_id,
							merchant_keys:'7034288755',
					        device_id:'XXX_1234567890_1230187',
					        device_platform:'android',
					        device_uiid:'uid_123',
					        code_version:'1.2',
					        lang:'en',
					        search_mode:'address',
					        location_mode:'1'
							},
						dataType:'json',
						success:function(result1)
							{
							if(result1.code == 1)
								{
								var items = result1.details.data;
								for (var j = 0; j < items.length; j++) 
									{
									if(favorite.indexOf(items[j].item_id) == -1)
										{
	    								var button = `<button type="submit" onclick="set_favorite(`+items[j].item_id+`,`+cat[i].cat_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i></button>&nbsp;`;
										} 
									else
										{
										var button = `<button type="submit" onclick="remove_favorite(`+items[j].item_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart" aria-hidden="true"></i></button>&nbsp;`;
										}
									if(cart.indexOf(items[j].item_id) == -1)
										{	
										button = button+`<button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" onclick="add_tocart(`+items[j].item_id+`,`+items[j].prices[0].price+`)">Add To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>`;
										}
									else
										{
										button = button+`<a href="cart.php" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small">Go To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>`;
										}
									html_product = html_product+`
<li class="span3">
	<div class="product-box">
		<span class="sale_tag"></span>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
			<img style="width:266px;height:275px" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border">
		</a>
		<br/>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`" class="title">`+items[j].item_name+`</a><br/>
		<a href="category.php?cat=`+cat[i].cat_id+`" class="category">`+cat[i].category_name+`</a>
		<p class="price">`+items[j].prices[0].formatted_price+`</p>`+button+`
	</div>
</li>`;
									}
								}
							}
					});
				}
			$('#category').html(html_cat);
			$('#products').html(html_product);
			}
		}
	});
});
function set_favorite(item_id,cat_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>AddFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				category_id:cat_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
function remove_favorite(item_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>RemoveFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html('Removed from your favorite list.');
					}
				$('#products').load('#products');
				}
			});
	}
function add_tocart(item_id,price)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			// url:'<?=$url;?>addToCart',
			// url:'cart.php',
			url:'<?=$url;?>addToCart?&merchant_keys=7034288755&device_id=XXX_1234567890_1230187&device_platform=android&device_uiid=uid_123&code_version=1.2&lang=en&search_mode=address&location_mode=1',
			method:'post',
			data:
				{
				item_id:parseInt(item_id),
				token:token,
				qty:1,
				price:parseFloat(price),
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
</script>
<?php
}
elseif($i == 2)
{
?>
<section class="main-content">
	<div class="row">						
		<div class="span12 col">								
			<ul class="thumbnails listing-products" id="category">
				
			</ul>
		</div>
	</div>
</section>
<script type="text/javascript">
$(document).ready(function() 
{
// load the category for listing the product
$.ajax({
	url:'<?=$url;?>loadCategory',
	async: false,
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			$('#category').html('');
			var html_cat = ``;
			var cat = result.details.data;
			for (var i = 0; i < cat.length; i++) 
				{
				if(cat[i].featured_cat == 1)
					{
					html_cat = html_cat+`
<li class="span3">
	<div class="product-box">
		<span class="sale_tag"></span>
		<a href="category.php?cat=`+cat[i].cat_id+`">
			<img style="width:266px;height:275px" src="`+cat[i].photo_url+`" alt="`+cat[i].category_name+`" class="uk-card-border">
		</a>
		<br/>
		<a href="category.php?cat=`+cat[i].cat_id+`" class="title">`+cat[i].category_name+`</a><br/>
	</div>
</li>`;
					}
				}
			$('#category').html(html_cat);
			}
		}
	});
})

</script>
<?php
}
elseif($i == 3)
{
?>
<section class="main-content">
	<div class="row">						
		<div class="span9">								
			<ul class="thumbnails listing-products" id="products">
				
			</ul>
		</div>
		<div class="span3 col">
			<div class="block">	
				<ul class="nav nav-list" id="category">
					
				</ul>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
var favorite = [];
var cart = [];
var token = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
	{
	?>
	token = "<?=$_SESSION['loggedin_user']['token'];?>";
	// get favorite list if logged in
	$.ajax({
			url:'<?=$url;?>ItemFavoritesList',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data;
					if(data.length > 0)
						{
						for (var i = 0;i < data.length; i++)
							{
							favorite.push(data[i].item_id);
							}
						}
					}
				}
		});
	// get the items in cart if logged in
	$.ajax({
			url:'<?=$url;?>loadCart',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				transaction_type:'delivery'
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data.item;
					var array = [];
					var array = Object.values(data);
					if(array.length > 0)
						{
						for (var i = 0;i < array.length; i++)
							{
							cart.push(array[i].item_id);
							}
						}
					}
				}
		});
	<?php 
	}
?>
$(document).ready(function() 
{
// load the category for listing the product
$.ajax({
	url:'<?=$url;?>loadCategory',
	async: false,
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			$('#category').html('');
			$('#products').html('');
			var cat = result.details.data;
			var html_cat = `<li class="nav-header">SUB CATEGORIES</li>`;
			var html_product = ``;
			for (var i = 0; i < cat.length; i++) 
				{
				html_cat = html_cat+`<li><a href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
				$.ajax({
						url:'<?=$url;?>loadItemByCategory',
						async: false,
						data:
							{
							cat_id:cat[i].cat_id,
							merchant_keys:'7034288755',
					        device_id:'XXX_1234567890_1230187',
					        device_platform:'android',
					        device_uiid:'uid_123',
					        code_version:'1.2',
					        lang:'en',
					        search_mode:'address',
					        location_mode:'1'
							},
						dataType:'json',
						success:function(result1)
							{
							if(result1.code == 1)
								{
								var items = result1.details.data;
								for (var j = 0; j < items.length; j++) 
									{
									if(items[j].deal_status == 1)
										{
										if(favorite.indexOf(items[j].item_id) == -1)
											{
	    									var button = `<button type="submit" onclick="set_favorite(`+items[j].item_id+`,`+cat[i].cat_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i></button>&nbsp;`;
											} 
										else
											{
											var button = `<button type="submit" onclick="remove_favorite(`+items[j].item_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart" aria-hidden="true"></i></button>&nbsp;`;
											}
										if(cart.indexOf(items[j].item_id) == -1)
											{	
											button = button+`<button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" onclick="add_tocart(`+items[j].item_id+`,`+items[j].prices[0].price+`)">Add To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>`;
											}
										else
											{
											button = button+`<a href="cart.php" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small">Go To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>`;
											}
										html_product = html_product+`
<li class="span3">
	<div class="product-box">
		<span class="sale_tag"></span>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
			<img style="width:266px;height:275px" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border">
		</a>
		<br/>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`" class="title">`+items[j].item_name+`</a><br/>
		<a href="category.php?cat=`+cat[i].cat_id+`" class="category">`+cat[i].category_name+`</a>
		<p class="price">`+items[j].prices[0].formatted_price+`</p>`+button+`
	</div>
</li>`;
										}
									}
								}
							}
					});
				}
			$('#category').html(html_cat);
			$('#products').html(html_product);
			}
		}
	});
});
function set_favorite(item_id,cat_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>AddFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				category_id:cat_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
function remove_favorite(item_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>RemoveFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html('Removed from your favorite list.');
					}
				$('#products').load('#products');
				}
			});
	}
function add_tocart(item_id,price)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			// url:'<?=$url;?>addToCart',
			// url:'cart.php',
			url:'<?=$url;?>addToCart?&merchant_keys=7034288755&device_id=XXX_1234567890_1230187&device_platform=android&device_uiid=uid_123&code_version=1.2&lang=en&search_mode=address&location_mode=1',
			method:'post',
			data:
				{
				item_id:parseInt(item_id),
				token:token,
				qty:1,
				price:parseFloat(price),
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
</script>
<?php
}
elseif($i == 4)
{
?>
<section class="main-content">
	<div class="row">						
		<div class="span9">								
			<ul class="thumbnails listing-products" id="products">
				
			</ul>
		</div>
		<div class="span3 col">
			<div class="block">	
				<ul class="nav nav-list" id="category">
					
				</ul>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
var favorite = [];
var cart = [];
var token = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
	{
	?>
	token = "<?=$_SESSION['loggedin_user']['token'];?>";
	// get favorite list if logged in
	$.ajax({
			url:'<?=$url;?>ItemFavoritesList',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data;
					if(data.length > 0)
						{
						for (var i = 0;i < data.length; i++)
							{
							favorite.push(data[i].item_id);
							}
						}
					}
				}
		});
	// get the items in cart if logged in
	$.ajax({
			url:'<?=$url;?>loadCart',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				transaction_type:'delivery'
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data.item;
					var array = [];
					var array = Object.values(data);
					if(array.length > 0)
						{
						for (var i = 0;i < array.length; i++)
							{
							cart.push(array[i].item_id);
							}
						}
					}
				}
		});
	<?php 
	}
?>
$(document).ready(function() 
{
// load the category for listing the product
$.ajax({
	url:'<?=$url;?>loadCategory',
	async: false,
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			$('#category').html('');
			$('#products').html('');
			var cat = result.details.data;
			var html_cat = `<li class="nav-header">SUB CATEGORIES</li>`;
			var html_product = ``;
			for (var i = 0; i < cat.length; i++) 
				{
				html_cat = html_cat+`<li><a href="category.php?cat=`+cat[i].cat_id+`">`+cat[i].category_name+`</a></li>`;
				$.ajax({
						url:'<?=$url;?>loadItemByCategory',
						async: false,
						data:
							{
							cat_id:cat[i].cat_id,
							merchant_keys:'7034288755',
					        device_id:'XXX_1234567890_1230187',
					        device_platform:'android',
					        device_uiid:'uid_123',
					        code_version:'1.2',
					        lang:'en',
					        search_mode:'address',
					        location_mode:'1'
							},
						dataType:'json',
						success:function(result1)
							{
							if(result1.code == 1)
								{
								var items = result1.details.data;
								for (var j = 0; j < items.length; j++) 
									{
									if(favorite.indexOf(items[j].item_id) == -1)
										{
										} 
									else
										{
										var button = `<button type="submit" onclick="remove_favorite(`+items[j].item_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart" aria-hidden="true"></i></button>&nbsp;`;
										if(cart.indexOf(items[j].item_id) == -1)
											{	
											button = button+`<button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" onclick="add_tocart(`+items[j].item_id+`,`+items[j].prices[0].price+`)">Add To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>`;
											}
										else
											{
											button = button+`<a href="cart.php" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small">Go To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>`;
											}
										html_product = html_product+`
<li class="span3">
	<div class="product-box">
		<span class="sale_tag"></span>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
			<img style="width:266px;height:275px" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border">
		</a>
		<br/>
		<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`" class="title">`+items[j].item_name+`</a><br/>
		<a href="category.php?cat=`+cat[i].cat_id+`" class="category">`+cat[i].category_name+`</a>
		<p class="price">`+items[j].prices[0].formatted_price+`</p>`+button+`
	</div>
</li>`;
										}
									}

								}
							}
					});
				}
			$('#category').html(html_cat);
			$('#products').html(html_product);
			}
		}
	});
});
function set_favorite(item_id,cat_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>AddFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				category_id:cat_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
function remove_favorite(item_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>RemoveFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html('Removed from your favorite list.');
					}
				$('#products').load('#products');
				}
			});
	}
function add_tocart(item_id,price)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			// url:'<?=$url;?>addToCart',
			// url:'cart.php',
			url:'<?=$url;?>addToCart?&merchant_keys=7034288755&device_id=XXX_1234567890_1230187&device_platform=android&device_uiid=uid_123&code_version=1.2&lang=en&search_mode=address&location_mode=1',
			method:'post',
			data:
				{
				item_id:parseInt(item_id),
				token:token,
				qty:1,
				price:parseFloat(price),
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
</script>
<?php
}
elseif($i == 5)
{
?>
<div class="uk-section uk-section-muted uk-padding-remove-top">
	<div class="uk-container uk-container-xlarge">
		<div class="uk-width-expand">
			<div uk-grid="" class="uk-grid">
				<form class="uk-search uk-search-default uk-search-large" method="GET" action="products.php" role="search" name="myForm" onsubmit="return validateForm()">
					<div class="uk-width-1-2 uk-first-column">
						<button uk-search-icon></button>
						<input type="hidden" name="i" value="5">
						<input class="uk-search-input uk-text-emphasis uk-border-pill" type="search" placeholder="Food You Wish To Have" name="search" value="<?=$_GET['search'];?>">
					</div> 
				</form>
			</div>
			<div class="uk-alert-success uk-alert" uk-alert="" id="msg" style="display: none;">
  				<a class="uk-alert-close uk-icon uk-close" uk-close=""></a>
  				<p id="message"></p>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<ul class="uk-switcher" style="touch-action: pan-y pinch-zoom;">
					<li class="uk-active" style="">
						<div class="uk-child-width-1-4@m uk-grid" uk-grid="" id="products">
							
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
<?php
if(isset($_GET['search']))
{
?>
var search = "<?=$_GET['search'];?>";
var favorite = [];
var cart = [];
var token = '';
<?php 
if(isset($_SESSION['loggedin_user']['token']))
	{
	?>
	token = "<?=$_SESSION['loggedin_user']['token'];?>";
	// get favorite list if logged in
	$.ajax({
			url:'<?=$url;?>ItemFavoritesList',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data;
					if(data.length > 0)
						{
						for (var i = 0;i < data.length; i++)
							{
							favorite.push(data[i].item_id);
							}
						}
					}
				}
		});
	// get the items in cart if logged in
	$.ajax({
			url:'<?=$url;?>loadCart',
			async: false,
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				token:token,
				transaction_type:'delivery'
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 1)
					{
					var data = result.details.data.item;
					var array = [];
					var array = Object.values(data);
					if(array.length > 0)
						{
						for (var i = 0;i < array.length; i++)
							{
							cart.push(array[i].item_id);
							}
						}
					}
				}
		});
	<?php 
	}
?>
$(document).ready(function() 
{
// load the category for listing the product
$.ajax({
	url:'<?=$url;?>loadCategory',
	async: false,
	data:
		{
		merchant_keys:'7034288755',
		device_id:'XXX_1234567890_1230187',
		device_platform:'android',
		device_uiid:'uid_123',
		code_version:'1.2',
		lang:'en',
		search_mode:'address',
		location_mode:'1'
		},
	dataType:'json',
	success:function(result)
		{
		if(result.code == 1)
			{
			$('#products').html('');
			var cat = result.details.data;
			var html_product = ``;
			for (var i = 0; i < cat.length; i++) 
				{
				$.ajax({
						url:'<?=$url;?>loadItemByCategory',
						async: false,
						data:
							{
							cat_id:cat[i].cat_id,
							merchant_keys:'7034288755',
					        device_id:'XXX_1234567890_1230187',
					        device_platform:'android',
					        device_uiid:'uid_123',
					        code_version:'1.2',
					        lang:'en',
					        search_mode:'address',
					        location_mode:'1'
							},
						dataType:'json',
						success:function(result1)
							{
							if(result1.code == 1)
								{
								var items = result1.details.data;
								for (var j = 0; j < items.length; j++) 
									{
									var str = items[j].item_name;
									var find = str.indexOf(search);
									if(find != -1)
										{
										if(favorite.indexOf(items[j].item_id) == -1)
											{
		    								var button = `<button type="submit" onclick="set_favorite(`+items[j].item_id+`,`+cat[i].cat_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart-o" aria-hidden="true"></i></button>&nbsp;`;
											} 
										else
											{
											var button = `<button type="submit" onclick="remove_favorite(`+items[j].item_id+`)" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" style="background-color: #af918b;!important;color:#523d3d!important">Favourite&nbsp;<i class="fa fa-heart" aria-hidden="true"></i></button>&nbsp;`;
											}
										if(cart.indexOf(items[j].item_id) == -1)
											{	
											button = button+`<button type="submit" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small" onclick="add_tocart(`+items[j].item_id+`,`+items[j].prices[0].price+`)">Add To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></button>`;
											}
										else
											{
											button = button+`<a href="cart.php" class="uk-button uk-button-secondary uk-border-pill uk-margin-top uk-button-small">Go To Cart &nbsp;<i class="fa fa-shopping-cart" aria-hidden="true"></i></a>`;
											}
										html_product = html_product+`
<div class="">
	<div class="uk-card uk-border-10 toggle uk-animation-toggle" tabindex="0">
		<div class="uk-card-media-top uk-list-three">
			<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`">
				<img style="width:266px;height:275px" src="`+items[j].photo_url+`" alt="`+items[j].item_name+`" class="uk-card-border">
			</a>
		</div>
		<div class="uk-list-two">
			<h3 class="uk-card-title">
				<a href="product_view.php?cat=`+cat[i].cat_id+`&product=`+items[j].item_id+`" class="uk-link-heading">`+items[j].item_name+`</a>
			</h3>
			<ul class="uk-comment-meta uk-subnav uk-subnav-divider uk-margin-remove-top">
				<li>`+items[j].prices[0].formatted_price+`</li>
				<li class="uk-margin-top">
					<a href="category.php?cat=`+cat[i].cat_id+`" class="uk-cat">`+cat[i].category_name+`</a>
				</li>
				<li class="uk-margin-top" id="bottons_`+items[j].item_id+`">`+button+`</li>
			</ul>
		</div>
	</div>
</div>`;
										}
									}
								}
							}
					});
				}
			$('#products').html(html_product);
			}
		}
	});
});
function set_favorite(item_id,cat_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>AddFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				category_id:cat_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
function remove_favorite(item_id)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			url:'<?=$url;?>RemoveFavorite',
			data:
				{
				merchant_keys:'7034288755',
				device_id:'XXX_1234567890_1230187',
				device_platform:'android',
				device_uiid:'uid_123',
				code_version:'1.2',
				lang:'en',
				search_mode:'address',
				location_mode:'1',
				item_id:item_id,
				token:token,
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html('Removed from your favorite list.');
					}
				$('#products').load('#products');
				}
			});
	}
function add_tocart(item_id,price)
	{
	if(token == '')
		{
		Swal.fire({
				type: "error",
				title: "Failed!",
				text: 'Please SignIn to continue!',
				timer: 3000,
				showConfirmButton: false,
				});
		return;
		}
	$.ajax({
			// url:'<?=$url;?>addToCart',
			// url:'cart.php',
			url:'<?=$url;?>addToCart?&merchant_keys=7034288755&device_id=XXX_1234567890_1230187&device_platform=android&device_uiid=uid_123&code_version=1.2&lang=en&search_mode=address&location_mode=1',
			method:'post',
			data:
				{
				item_id:parseInt(item_id),
				token:token,
				qty:1,
				price:parseFloat(price),
				},
			dataType:'json',
			success:function(result)
				{
				if(result.code == 0)
					{
					Swal.fire({
								type: "error",
								title: "Failed!",
								text: result.msg,
								timer: 3000,
								showConfirmButton: false,
								});
					}
				else if(result.code == 1)
					{
					$('#msg').show();
					$('#message').html('');
					$('#message').html(result.msg);
					}
				$('#products').load('#products');
				}
			});
	}
<?php 
}
?>
</script>
<?php
}
?>
</div>
<script type="text/javascript">
$(document).ready(function() 
{ 
$.ajax({
        url:'<?=$url;?>getMerchantInfo',
        data:
        	{
          	merchant_keys:'7034288755',
          	device_id:'XXX_1234567890_1230187',
          	device_platform:'android',
          	device_uiid:'uid_123',
          	code_version:'1.2',
          	lang:'en',
          	search_mode:'address',
          	location_mode:'1'
          	},
        dataType:'json',
        success:function(result)
          {
          if(result.code == 1)
            {
            $('.pageBanner').attr('alt',result.details.data.merchant_name);
            $('.pageBanner').attr('src',result.details.data.logo);
            }
          else
            location.reload();
          }
      });
})
</script>
<?php
include('footer.php');
?>